# cnote.info

The website for cnote.info.

This is a Vue Press site, built and deployed from a Gitlab CI pipeline to an AWS S3 bucket hosted behind a CloudFront distribution with a little bit of logic in a couple of Lambda@Edge functions.