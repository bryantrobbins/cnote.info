# Flute Lessons with Christine

Christine has been teaching flute lessons since 2005 to students of all ages. She would
love to help you or your student take flute musicianship and performance to the next level,
at a reasonable cost and a schedule that works for you.

Keep reading below to get a feel for how our private lessons work. When you're
ready, [tell us a little more about yourself and your student](https://forms.gle/FM4Byf5pi4VXkt2z9)
to set up a free, no-pressure trial lesson.

The expectations of the instructor, student, and parent/guardian are outlined in our Private Flute Lessons
Policy document. The purpose of the policy is to clarify these expectations and define procedures that support
the effective administration of lessons. You can view the complete policy (revised for August
2019) [in this PDF document](./assets/lessons-2019.pdf).

## Location, Schedule, and Cost

Generally, Christine prefers to teach private lessons:

* Out of her home
* In 30-minute, weekly time slots
* For $75 / month (as of August 2019)

Teaching out of her home allows Christine to keep on a tighter schedule and avoid distractions for the student. The 30-minute
time slots require punctuality and focus without replacing the daily discipline of practicing. As of Fall 2019, the price
of $75 / month is certainly below market value for private musical instruction. We'll review the costs and demand at the beginning
each school year, but we value your relationship much more than your checkbook!

There will also be supplemental costs for materials such as method books.

Importantly, all of these details are negotiable to fit your situation. Don't let location, scheduling,
or cost prevent you from contacting us and working out something that works for you.

## Lesson Format

Christine develops customized lesson plans for each student, built around classic foundations like sight reading,
scales, technical drills, progressive exercises, and performance pieces.

## Lesson Contract

After a successful trial lesson and decision to start private lessons, the Instructor, Student, and Parent/Guardian sign a written agreement called a "Lesson Contract". The signed Lesson Contract indicates that all three parties:

* Agree to hold one accountable under the expectations and procedures outlined in the Policy
* Agree to a specific lesson time, schedule, and location for lessons

