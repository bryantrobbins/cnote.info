# About Christine

![A picture of Christine Robbins](./about-image.jpg)

Chrsitine Robbins is a lifelong lover of music. She has played the flute from a young age, and been teaching lessons to students of all ages since 2005. She has played in marching bands, concert bands, small ensembles, orchestras, and other musical groups since her playing began at age
10.

A native of Goose Creek, SC, Christine earned a B.A. in Music in 2008 from Mississippi State University. After several years teaching flute and children's music in the DC area,
she and her husband Bryan now live in Madison, AL.

Outside of music, Christine enjoys working out, volunteering at church, and cooking up new things in the kitchen.