# Working with Groups

For Christine, group musical performances represent an opportunity for every student to grow.
The power of being a part of something bigger than yourself means the responsibility of
being accountable to your peers. As a student and band member herself, these challenges still
drive her.

Does your band or other group have a need for help with flute auditions, sectional instruction,
marching band rehearsals, or other areas? Christine is ready to help, even on a volunteer basis.

[Tell us more about your opportunity](https://forms.gle/jtqokVowThiwiG4M7) and let's work together
to give your students even better opportunities to succeed.
