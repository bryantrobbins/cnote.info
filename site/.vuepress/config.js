module.exports = {
  title: 'CNote Studios',
  description: 'Flute with Christine Robbins in Madison, AL',
  dest: 'dist',
  themeConfig: {
    nav: [
      { text: 'Home', link: '/' },
      { text: 'About Christine', link: 'about' },
      { text: 'Lessons', link: 'lessons' },
      { text: 'Groups', link: 'groups' },
      { text: 'Contact', link: 'contact' }
    ]
  },
  ga: 'UA-145126622-1'
}