---
home: true
heroImage: /home-image.JPG
actionText: Contact Christine Today!
actionLink: contact
footer: Copyright © 2019 Christine Robbins
---

<div class="features">
  <div class="feature">
    <h2>Experience</h2>
    <p><a href="/about.html" class>Christine is a classically trained flute teacher and performer</a>, with several years of experience teaching students of all ages.</p>
  </div>
  <div class="feature">
    <h2>Private Lessons</h2>
    <p><a href="/lessons.html">Christine's private flute lessons</a> emphasize the fundamentals of musicianship, leveraging proven teaching techniques to foster exciting individual progress.</p>
  </div>
  <div class="feature">
    <h2>Groups</h2>
    <p>For Fall 2019, <a href="/groups.html">Christine is available as a volunteer assistant</a> to help your band or musical group excel.</p>
  </div>
</div>