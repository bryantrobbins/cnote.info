# Contact Christine

Contact Christine:

* [With general questions or comments](https://forms.gle/9Q7hVyz76hRcjYHs8)
* [To set up a free trial flute lesson](https://forms.gle/FM4Byf5pi4VXkt2z9)
* [About a volunteer or paid opportunity with your band or group](https://forms.gle/jtqokVowThiwiG4M7)

We can't wait to hear from you!